
<section id="Services" class="container-fluid">
    <div class="">

            <div class="row AllServices">
                <div class="col-12">
                    <h2 class="h2_class">Nos services</h2>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 col-6">
                    <div class="livraison service_width text-center ">
                        <div class="h_img">
                            <img src="img/footer_dluxxix/livraison_icon.png" alt="Livraison Gratuite">
                        </div>
                        <p class="p_service">Livraison Gratuite </p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 col-6">
                    <div class="paiement service_width text-center">
                        <div class="h_img">
                            <img src="img/footer_dluxxix/p_securisé.png" alt="Paiment Sécurisé">
                        </div>
                        <p class="p_service">Paiment Sécurisé</p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 col-6">
                    <div class="suivi service_width text-center">
                        <div class="h_img">
                            <img src="img/footer_dluxxix/suivi_cmd.png" alt="Suivi de commande">
                        </div>
                        <p class="p_service">Suivi de commande</p>
                    </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 col-6">
                        <div class="suivi service_width text-center">
                        <div class="h_img">
                            <img src="img/footer_dluxxix/reclamation.png" alt="Réclamation">
                        </div>
                            <p class="p_service">Réclamation</p>
                        </div>
                    </div>
                </div>
            </div>
</section>
<footer class="container-fluid">
<div class="">
            <div class="logo_footer">
                <a href="#"><img src="img/footer_dluxxix/logo_footer.png" id="footer_img_logo"  alt="logo footer"></a>
            </div>
            <div class="row my_footer">

                <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12 ">
                    <div class="row store_div">
                        <div class=" col-lg-6 col-sm-6 col-xs-6 col-6">
                            <span>
                                <img src="img/footer_dluxxix/app-store.png"  alt="app store" class="img_store">
                            </span>
                            <span>Apple Store</span>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 col-6">
                            <span>
                                <img src="img/footer_dluxxix/google-play.png"  alt="google play"  class="img_store">
                            </span>
                            <span>Google Store</span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 col-2 text-center div_img_p">
                            <img src="img/footer_dluxxix/visa.png" class="img_p"  alt="visa">
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 col-2 text-center div_img_p">
                            <img src="img/footer_dluxxix/p_card.png"  class="img_p"  alt="payment card">
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 col-2 text-center div_img_p">
                            <img src="img/footer_dluxxix/outline_diners.png"  class="img_p"  alt="outline diners">
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 col-2 text-center div_img_p">
                            <img src="img/footer_dluxxix/p_mobile.png"  class="img_p"  alt="payment mobile">
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 col-2 text-center div_img_p">
                            <img src="img/footer_dluxxix/master_card.png"  class="img_p"  alt="master card">
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 col-2 text-center div_img_p" >
                            <img src="img/footer_dluxxix/paypal.png" class="img_p"  alt="paypal"> 
                        </div>
                    </div>
                </div>

                <div class="col-lg-1 col-md-2 col-sm-4 col-xs-4 col-4">
                    <ul class="list_title listy1">
                        <li><a href="#">ACCUEIL</a></li>
                    </ul>
                    <ul class="listy1">
                        <li class="list_item"><a href="#">CATG 1</a></li>
                        <li class="list_item"><a href="#">CATG 2</a></li>
                        <li class="list_item"><a href="#">CATG 3</a></li>
                        <li class="list_item"><a href="#">CATG 4</a></li>
                        <li class="list_item"><a href="#">CATG 5</a></li>
                        <li class="list_item"><a href="#">CATG 6</a></li>
                    </ul>
                </div>

                <div class="col-lg-3 col-md-4 col-sm-8 col-xs-8 col-8">
                    <ul class="list_title listy2">
                        <li><a href="#">ESPACE CLIENT</a></li>
                    </ul>
                    <ul class="listy2">
                        <li class="list_item"><a href="blog.php">BLOG</a></li>
                        <li class="list_item"><a href="contact.php">CONTACT</a></li>
                        <li class="list_item"><a href="#">MENTIONS LEGALES</a></li>
                        <li class="list_item"><a href="#">DROIT DE RETRACTION</a></li>
                        <li class="list_item"><a href="#">CONDDITION DE VENTE</a> </li>
                        <li class="list_item"><a href="#">MODE DE LIVRISON</a> </li>
                    </ul>
                </div>

                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                    <div>
                        <p class="soc_p">Réseaux sociaux</p>
                    </div>
                    <div class="rs_div">
                        <a href="#"><img src="img/footer_dluxxix/facebook.png" class="img_soc"  alt="facebook"></a>
                        
                        <a href="#"><img src="img/footer_dluxxix/linkedin.png"  class="img_soc"  alt="linkedin"></a>
                        
                        <a href="#"><img src="img/footer_dluxxix/instagram.png"  class="img_soc"  alt="instagram"></a>
                        
                        <a href="#"><img src="img/footer_dluxxix/printerest.png" class="img_soc"  alt="printerest" ></a>
                        
                        <a href="#"><img src="img/footer_dluxxix/twitter.png" class="img_soc" alt="twitter"></a>
                    </div>
                </div>
            </div>

                <div class="row dr_block">
                <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 ">
                    <div>
                    <h6 class="h6_footer">INSCRIVEZ-VOUS À NOTRE NEWSLETTER</h6>
                    <p class="p_footer">Obtenez toutes les dernières informations sur les événements, les ventes<br>
                        et les offres. Inscrivez-vous à la newsletter dès aujourd'hui.</p>
                    </div>
                </div>
                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 col-12 news_div">
                    <div class="newsletter">
                        <img src="img/footer_dluxxix/mail_adress.png" alt="mail adress">
                        <input type="email" placeholder="Votre email" name="email" required="required">
                        <button type="submit" class="bt-nl">Souscrire</button>
                    </div>
                </div>
            </div>




        </div>
    </footer>
        <!-- bootstrap-4 -->
  