<section id="articles-bureautiques">
        <div class="container-fluid">
            <h2 class="h2_class">Nos articles bureautiques</h2>
            <div class="owl-carousel">
                <div>
                    <div class="one-article" id="img-box">
                    <div class="article-img">
                        <img src="img/meuble-bureautiques.png" alt="" class="img-fluid">
                    </div>
                        <div class="article-hover">
                            <div class="article-icon">
                                <div><a><img class="addwishlist" src="img/whislist-icon.png"></a></div>
                                <div><a href="#"><img src="img/lock-icon.png"></a></div>
                                <div><a href="#"><img src="img/cart-icon.png"></a></div>
                            </div>
                            <div class="article-status">
                                <h3>Disponible</h3>
                            </div>
                            <div class="article-category">
                                <h4>Catégorie A</h4>
                            </div>
                        </div>
                                        </div>
                                    </div>
                <div>
                    <div class="one-article" id="img-box">
                    <div class="article-img">
                        <img src="img/meuble-bureautiques1.png" alt="" class="img-fluid">
</div>
                        <div class="article-hover">
        <div class="article-icon">
          <div><a><img class="addwishlist" src="img/whislist-icon.png"></a></div>
          <div><a href="#"><img src="img/lock-icon.png"></a></div>
          <div><a href="#"><img src="img/cart-icon.png"></a></div>
        </div>
        <div class="article-status">
          <h3>Disponible</h3>
        </div>
        <div class="article-category">
          <h4>Catégorie A</h4>
        </div>
      </div>
                    </div>
                </div>
                <div>
                    <div class="one-article" id="img-box">
                    <div class="article-img">
                        <img src="img/meuble-bureautiques2.png" alt="" class="img-fluid">
</div>
                        <div class="article-hover">
        <div class="article-icon">
          <div><a><img class="addwishlist" src="img/whislist-icon.png"></a></div>
          <div><a href="#"><img src="img/lock-icon.png"></a></div>
          <div><a href="#"><img src="img/cart-icon.png"></a></div>
        </div>
        <div class="article-status">
          <h3>Disponible</h3>
        </div>
        <div class="article-category">
          <h4>Catégorie A</h4>
        </div>
      </div>
                    </div>
                </div>
                <div>
                    <div class="one-article" id="img-box">
                    <div class="article-img">
                        <img src="img/meuble-bureautiques3.png" alt="" class="img-fluid">
</div>
                        <div class="article-hover">
        <div class="article-icon">
          <div><a><img class="addwishlist" src="img/whislist-icon.png"></a></div>
          <div><a href="#"><img src="img/lock-icon.png"></a></div>
          <div><a href="#"><img src="img/cart-icon.png"></a></div>
        </div>
        <div class="article-status">
          <h3>Disponible</h3>
        </div>
        <div class="article-category">
          <h4>Catégorie A</h4>
        </div>
      </div>
                    </div>
                </div>
              </div>
        </div>
    </section>