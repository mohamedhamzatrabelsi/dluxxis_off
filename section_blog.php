<div class="container-fluid">
<h2 class="h2_class">Actualités</h2>
<div class="row">
<div class="blog1 col-lg-5 col-md-5 col-sm-12 col-xs-12">
<h3 class="h3_class">Stanley</h3>
<h5 class="h5_class">Carver Dining Chair, Bay Green & Black</h5>
<p>
Ready for a new kind of dining? These new season pieces are designed to be versatile, where nothing has to match, but everything just goes.
</p>
<a href="#img-blog1">Lire Plus...</a>
<div class="image-div">
<img src="img/Rectangle37.png" id="img-blog1" />
</div>
</div>
<div class="blog2 col-lg-5 col-md-5 col-sm-12 col-xs-12">
<h3 class="h3_class">Stanley</h3>
<h5 class="h5_class">Carver Dining Chair, Bay Green & Black</h5>
<p>
Ready for a new kind of dining? These new season pieces are designed to be versatile, where nothing has to match, but everything just goes.
</p>
<a href="#img-blog2">Lire Plus...</a>
<div class="image-div">
<img src="img/Rectangle68.png" id="img-blog2" />
</div>
</div>
</div>

</div>