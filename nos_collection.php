<section id="nos-collections">
        <div class="container-fluid">
            <h2 class="h2_class">Nos collections</h2>
            <div class="owl-carousel owl-theme">
                <div class="text-center">
                    <span class="nos-collections-first">
                        <div class="img-box" id="img-box">
                            <div>
                                <img src="img/miroir.png" class="img-fluid" alt="chambre à coucher">
                            </div>
                            <div class="box-content">
                                <ul class="icon">
                                    <li><a href="#"><img src="img/cart-collection.png"></a></li>
                                    <li><a  data-toggle="modal" data-target="#exampleModalCenter"><img src="img/searsh-collection.png"></a></li> 
                                    <li><a ><img class="addwishlist" src="img/whishlist-collection.png"></a></li>
                                </ul>
                                <a href="./fiche-produit.php"><h3 class="title small">Chambre à coucher</h3></a>
                            </div>
                        </div>
                        <div class="img-box" id="img-box">
                            <div>
                                <img src="img/tele.png" class="img-fluid" alt="chambre à coucher">
                            </div>
                            <div class="box-content">
                            <ul class="icon">
                                    <li><a href="#"><img src="img/cart-collection.png"></a></li>
                                    <li><a data-toggle="modal" data-target="#exampleModalCenter"><img src="img/searsh-collection.png"></a></li>
                                    <li><a ><img  class="addwishlist" src="img/whishlist-collection.png"></a></li>
                                </ul>
                                <a href="./fiche-produit.php"><h3 class="title small">Chambre à coucher</h3></a>
                            </div>
                        </div>
                    </span>
                    <span class="nos-collections-second">
                        <div class="img-box" id="img-box">
                            <img src="img/chambre.png" class="img-fluid" alt="chambre à coucher">
                            <div class="box-content">
                            <ul class="icon">
                                    <li><a href="#"><img src="img/cart-collection.png"></a></li>
                                    <li><a data-toggle="modal" data-target="#exampleModalCenter"><img src="img/searsh-collection.png"></a></li>
                                    <li><a><img class="addwishlist"  src="img/whishlist-collection.png"></a></li>
                                </ul>
                            
                                <a href="./fiche-produit.php"><h3 class="title">Chambre à coucher</h3></a>
                            </div>
                        </div>
                    </span>
                    <span class="nos-collections-third">
                        <div class="img-box" id="img-box">
                            <div>
                                <img src="img/canappe.png" class="img-fluid" alt="chambre à coucher">
                            </div>
                            <div class="box-content">
                            <ul class="icon">
                                    <li><a href="#"><img src="img/cart-collection.png"></a></li>
                                    <li><a data-toggle="modal" data-target="#exampleModalCenter"><img src="img/searsh-collection.png"></a></li>
                                    <li><a ><img  class="addwishlist" src="img/whishlist-collection.png"></a></li>
                                </ul>
                                <a href="./fiche-produit.php"><h3 class="title small">Chambre à coucher</h3></a>
                            </div>
                        </div>
                        <div class="img-box" id="img-box">
                            <div>
                                <img src="img/lit.png" class="img-fluid" alt="chambre à coucher">
                            </div>
                            <div class="box-content">
                            <ul class="icon">
                                    <li><a href="#"><img src="img/cart-collection.png"></a></li>
                                    <li><a data-toggle="modal" data-target="#exampleModalCenter"><img src="img/searsh-collection.png"></a></li>
                                    <li><a><img class="addwishlist"  src="img/whishlist-collection.png"></a></li>
                                </ul>
                                <a href="./fiche-produit.php"><h3 class="title small">Chambre à coucher</h3></a>
                            </div>
                        </div>
                    </span>
                </div>
                <div class="text-center">
                    <span class="nos-collections-first">
                        <div class="img-box" id="img-box">
                            <div>
                                <img src="img/office_vue2.png" class="img-fluid" alt="meubles bureau">
                            </div>
                            <div class="box-content">
                            <ul class="icon">
                                    <li><a href="#"><img src="img/cart-collection.png"></a></li>
                                    <li><a data-toggle="modal" data-target="#exampleModalCenter"><img src="img/searsh-collection.png"></a></li>
                                    <li><a ><img class="addwishlist"  src="img/whishlist-collection.png"></a></li>
                                </ul>
                                <a href="./fiche-produit.php"><h3 class="title small">Meubles bureau</h3></a>
                            </div>
                        </div>
                        <div class="img-box" id="img-box">
                            <div>
                                <img src="img/office_vue3.png" class="img-fluid" alt="meubles bureau">
                            </div>
                            <div class="box-content">
                            <ul class="icon">
                                    <li><a href="#"><img src="img/cart-collection.png"></a></li>
                                    <li><a data-toggle="modal" data-target="#exampleModalCenter"><img src="img/searsh-collection.png"></a></li>
                                    <li><a><img  class="addwishlist" src="img/whishlist-collection.png"></a></li>
                                </ul>
                                <a href="./fiche-produit.php"><h3 class="title small">Meubles bureau</h3></a>
                            </div>
                        </div>
                    </span>
                    <span class="nos-collections-second">
                        <div class="img-box" id="img-box">
                            <img src="img/office1.png" class="img-fluid" alt="meubles bureau">
                            <div class="box-content">
                            <ul class="icon">
                                    <li><a href="#"><img src="img/cart-collection.png"></a></li>
                                    <li><a data-toggle="modal" data-target="#exampleModalCenter"><img src="img/searsh-collection.png"></a></li>
                                    <li><a ><img  class="addwishlist" src="img/whishlist-collection.png"></a></li>
                                </ul>
                                <a href="./fiche-produit.php"><h3 class="title">Meubles bureau</h3></a>
                            </div>
                        </div>
                    </span>
                    <span class="nos-collections-third">
                        <div class="img-box" id="img-box">
                            <div>
                                <img src="img/office_vue4.png" class="img-fluid" alt="meubles bureau">
                            </div>
                            <div class="box-content">
                            <ul class="icon">
                                    <li><a href="#"><img src="img/cart-collection.png"></a></li>
                                    <li><a data-toggle="modal" data-target="#exampleModalCenter"><img src="img/searsh-collection.png"></a></li>
                                    <li><a><img  class="addwishlist"  src="img/whishlist-collection.png"></a></li>
                                </ul>
                                <a href="./fiche-produit.php"><h3 class="title small">Meubles bureau</h3></a>
                            </div>
                        </div>
                        <div class="img-box" id="img-box">
                            <div>
                                <img src="img/office_vue1.png" class="img-fluid" alt="meubles bureau">
                            </div>
                            <div class="box-content">
                            <ul class="icon">
                                    <li><a href="#"><img src="img/cart-collection.png"></a></li>
                                    <li><a data-toggle="modal" data-target="#exampleModalCenter"><img src="img/searsh-collection.png"></a></li>
                                    <li><a><img  class="addwishlist"  src="img/whishlist-collection.png"></a></li>
                                </ul>
                                <a href="./fiche-produit.php"><h3 class="title small">Meubles bureau</h3></a>
                            </div>
                        </div>
                    </span>
                </div>

                <div class="text-center">
                    <span class="nos-collections-first">
                        <div class="img-box" id="img-box">
                            <div>
                                <img src="img/p_salon_vue.png" class="img-fluid" alt="Séjour & Element tv">
                            </div>
                            <div class="box-content">
                            <ul class="icon">
                                    <li><a href="#"><img src="img/cart-collection.png"></a></li>
                                    <li><a data-toggle="modal" data-target="#exampleModalCenter"><img src="img/searsh-collection.png"></a></li>
                                    <li><a><img  class="addwishlist" src="img/whishlist-collection.png"></a></li>
                                </ul>
                                <a href="./fiche-produit.php"><h3 class="title small">Element tv</h3></a>
                            </div>
                        </div>
                        <div class="img-box" id="img-box">
                            <div>
                                <img src="img/p_salon_vue1.png" class="img-fluid" alt="Séjour & Element tv">
                            </div>
                            <div class="box-content">
                            <ul class="icon">
                                    <li><a href="#"><img src="img/cart-collection.png"></a></li>
                                    <li><a data-toggle="modal" data-target="#exampleModalCenter"><img src="img/searsh-collection.png"></a></li>
                                    <li><a><img  class="addwishlist" src="img/whishlist-collection.png"></a></li>
                                </ul>
                                <a href="./fiche-produit.php"><h3 class="title small">Element tv</h3></a>
                            </div>
                        </div>
                    </span>
                    <span class="nos-collections-second">
                        <div class="img-box" id="img-box">
                            <img src="img/p_salon.png" class="img-fluid" alt="Séjour & Element tv">
                            <div class="box-content">
                            <ul class="icon">
                                    <li><a href="#"><img src="img/cart-collection.png"></a></li>
                                    <li><a data-toggle="modal" data-target="#exampleModalCenter"><img src="img/searsh-collection.png"></a></li>
                                    <li><a><img  class="addwishlist"  src="img/whishlist-collection.png"></a></li>
                                </ul>
                                <a href="./fiche-produit.php"><h3 class="title">Element tv</h3></a>
                            </div>
                        </div>
                    </span>
                    <span class="nos-collections-third">
                        <div class="img-box" id="img-box">
                            <div>
                                <img src="img/p_salon_vue2.png" class="img-fluid" alt="Séjour & Element tv">
                            </div>
                            <div class="box-content">
                            <ul class="icon">
                                    <li><a href="#"><img src="img/cart-collection.png"></a></li>
                                    <li><a data-toggle="modal" data-target="#exampleModalCenter"><img src="img/searsh-collection.png"></a></li>
                                    <li><a><img  class="addwishlist"  src="img/whishlist-collection.png"></a></li>
                                </ul>
                                <a href="./fiche-produit.php"><h3 class="title small">Element tv</h3></a>
                            </div>
                        </div>
                        <div class="img-box" id="img-box">
                            <div>
                                <img src="img/p_salon_vue3.png" class="img-fluid" alt="Séjour & Element tv">
                            </div>
                            <div class="box-content">
                            <ul class="icon">
                                    <li><a href="#"><img src="img/cart-collection.png"></a></li>
                                    <li><a data-toggle="modal" data-target="#exampleModalCenter"><img src="img/searsh-collection.png"></a></li>
                                    <li><a><img  class="addwishlist"  src="img/whishlist-collection.png"></a></li>
                                </ul>
                                <a href="./fiche-produit.php"><h3 class="title small">Element tv</h3></a>
                            </div>
                        </div>
                    </span>
                </div>
                
                <div class="text-center">
                    <span class="nos-collections-first">
                        <div class="img-box" id="img-box">
                            <div>
                                <img src="img/bar_comptoire_vue1.png" class="img-fluid" alt="bar_comptoire">
                            </div>
                            <div class="box-content">
                                <ul class="icon">
                                    <li><a href="#"><img src="img/cart-collection.png"></a></li>
                                    <li><a  data-toggle="modal" data-target="#exampleModalCenter"><img src="img/searsh-collection.png"></a></li> 
                                    <li><a ><img class="addwishlist" src="img/whishlist-collection.png"></a></li>
                                </ul>
                                <a href="./fiche-produit.php"><h3 class="title small">bar comptoire</h3></a>
                            </div>
                        </div>
                        <div class="img-box" id="img-box">
                            <div>
                                <img src="img/bar_comptoire_vue2.png" class="img-fluid" alt="bar_comptoire">
                            </div>
                            <div class="box-content">
                            <ul class="icon">
                                    <li><a href="#"><img src="img/cart-collection.png"></a></li>
                                    <li><a data-toggle="modal" data-target="#exampleModalCenter"><img src="img/searsh-collection.png"></a></li>
                                    <li><a ><img  class="addwishlist" src="img/whishlist-collection.png"></a></li>
                                </ul>
                                <a href="./fiche-produit.php"><h3 class="title small">bar comptoire</h3></a>
                            </div>
                        </div>
                    </span>
                    <span class="nos-collections-second">
                        <div class="img-box" id="img-box">
                            <img src="img/bar_comptoire.png" class="img-fluid" alt="bar_comptoire">
                            <div class="box-content">
                            <ul class="icon">
                                    <li><a href="#"><img src="img/cart-collection.png"></a></li>
                                    <li><a data-toggle="modal" data-target="#exampleModalCenter"><img src="img/searsh-collection.png"></a></li>
                                    <li><a><img class="addwishlist"  src="img/whishlist-collection.png"></a></li>
                                </ul>
                            
                                <a href="./fiche-produit.php"><h3 class="title">bar comptoire</h3></a>
                            </div>
                        </div>
                    </span>
                    <span class="nos-collections-third">
                        <div class="img-box" id="img-box">
                            <div>
                                <img src="img/bar_comptoire_vue3.png" class="img-fluid" alt="bar_comptoire">
                            </div>
                            <div class="box-content">
                            <ul class="icon">
                                    <li><a href="#"><img src="img/cart-collection.png"></a></li>
                                    <li><a data-toggle="modal" data-target="#exampleModalCenter"><img src="img/searsh-collection.png"></a></li>
                                    <li><a ><img  class="addwishlist" src="img/whishlist-collection.png"></a></li>
                                </ul>
                                <a href="./fiche-produit.php"><h3 class="title small">bar comptoire</h3></a>
                            </div>
                        </div>
                        <div class="img-box" id="img-box">
                            <div>
                                <img src="img/bar_comptoire_vue4.png" class="img-fluid" alt="bar_comptoire">
                            </div>
                            <div class="box-content">
                            <ul class="icon">
                                    <li><a href="#"><img src="img/cart-collection.png"></a></li>
                                    <li><a data-toggle="modal" data-target="#exampleModalCenter"><img src="img/searsh-collection.png"></a></li>
                                    <li><a><img class="addwishlist"  src="img/whishlist-collection.png"></a></li>
                                </ul>
                                <a href="./fiche-produit.php"><h3 class="title small">bar comptoire</h3></a>
                            </div>
                        </div>
                    </span>
                </div>


            </div>
        </div>
        </section>
      

        
<div class="modal fade pad_off" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog" role="document" >
                <div class="modal-content contanty_modal" id="img-box">
                            <div class="modal-body body_contente_modal">
                            <button type="button" class="close close_btn" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">
                                    <img src="img/exit-icon.png" id="img_exit">
                                </span>
                            </button>

                        <div class="col-lg-9 col-md-8 col-sm-12 col-xs-12 col-12" id="w_b1">
                            <h3 class="h3_quickview">Chambre à coucher</h3>
                            <div>
                                <img src="img/Rectangle 9.png" class="img-fluid">
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-4 col-sm-12 col-xs-12 col-12" id="w_b2">
                            <div class="contenty">
                                <h6 class="h6_quickview">Disponibilté : Disponible </h6>
                                <h6 class="h6_quickview">Prix :</h6>
                                <p class="p_quickview">LOREM IPSUM IS SIMPLY DUMMY TEXT OF THE PRINTING
                                    AND TYPESETTING INDUSTRY. LOREM IPSUM HAS BEEN
                                    THE INDUSTRY'S STANDARD DUMMY TEXT EVER SINCE
                                    THE 1500S, WHEN AN UNKNOWN PRINTER TOOK A
                                    GALLEY OF TYPE AND SCRAMBLED IT TO MAKE A TYPE
                                    SPECIMEN BOOK. IT HAS SURVIVED NOT ONLY FIVE
                                    CENTURIES, BUT ALSO THE LEAP INTO ELECTRONIC
                                    TYPESETTING, REMAINING ESSENTIALLY UNCHANGED. IT
                                    WAS POPULARISED IN THE 1960S WITH THE RELEASE OF
                                    LETRASET SHEETS CONTAINING LOREM IPSUM PASSAGES, AND MORE RECENTLY WITH DESKTOP PUBLISHING SOFTWARE LIKE ALDUS PAGEMAKER INCLUDING
                                    VERSIONS OF LOREM IPSULOREM IPSUM IS SIMPLY
                                    DUMMY TEXT OF THE PRINTING AND TYPESETTING
                                    INDUSTRY. LOREM IPSUM HAS BEEN THE INDUSTRY'S
                                    STANDARD DUMMY TEXT EVER</p>
                                    <h6 class="h6_quickview">Type de produit :</h6>
                                    <h6 class="h6_quickview">Choisissez la Quantité :</h6>
                                    
                                    <!--quantité manque-->
                                    <div class="quantity">
                                        <div class="flex1">
                                            <div class="flex2">
                                              <button  id="decrease_t" class="btn-counter" onclick="this.parentNode.querySelector('input[type=number]').stepDown()">-</button>
                                              <input type="number" id="qty_number" value="1"  min="1">
                                               <button  id="increase_t" class="btn-counter" onclick="this.parentNode.querySelector('input[type=number]').stepUp()">+</button>
                                              
                                            </div>
                                        </div>
                                    </div>
            
                                    <div id="chariot"><a href="#">Ajouter au chariot</a></div>
                                    <div id="categ"><a href="#">Même catégorie</a></div>
                                    <div id="fav">
                                        <a class="addwishlist">
                                            Ajouter au favoris
                                            <img  src="img/Groupe 1.png" id="img_fav">
                                        </a>
                                    </div>
                            </div>
                        </div>
                    </div>
            
                </div>
            </div>
