<!DOCTYPE html>
<html lang="en">

<head>
<head>
<link rel="stylesheet" href="css/bootstrap.min.css">
    <!--style css -->
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">
    <link rel="stylesheet" href="css/bannerCarrousel.css">
   <link rel="stylesheet" href="css/nos_collections.css">
    <link rel="stylesheet" href="css/fiche-produit.css">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, minimum-scale=1">
    <title>dluxxis</title>

   
</head>


</head>

<body>
    <div class="heady">
        <?php include("header.php"); ?>
    </div>
    <div class="header-mobile">
        <?php include("header-mobile.php"); ?>
    </div>
    <section id="banner_carousel">
        <?php include("bannerCarrousel.php"); ?>
    </section>
    <section class="container">
        <?php include("slider.php"); ?>
    </section>
    <section id="images-produit" class="container">
        <div class="row">
            <div class="col-lg-3 col-md-6 col-12 text-center">
                <img class="img-fluid" src="img/image-produit1.png" alt="image produit">
            </div>
            <div class="col-lg-3 col-md-6 col-12 text-center">
                <img class="img-fluid" src="img/image-produit2.png" alt="image produit">
            </div>
            <div class="col-lg-3 col-md-6 col-12 text-center">
                <img class="img-fluid" src="img/image-produit3.png" alt="image produit">
            </div>
            <div class="col-lg-3 col-md-6 col-12 text-center">
                <img class="img-fluid" src="img/image-produit4.png" alt="image produit">
            </div>
        </div>
    </section>

    <section id="details-produit  my-20" class="container-fluid" >
        <div class="row mx-0" id="img-box">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-8">
                        <h1 class="title7">Chambre à coucher</h1>
                    </div>
                    <div class="col-lg-4"><span class="prix title7 float-left">Prix :</span></div>
                    <p class="description">
                        LOREM IPSUM IS SIMPLY DUMMY TEXT OF THE PRINTING AND TYPESETTING INDUSTRY. LOREM IPSUM HAS BEEN
                        THE INDUSTRY'S STANDARD DUMMY TEXT EVER SINCE THE 1500S, WHEN AN UNKNOWN PRINTER TOOK A GALLEY
                        OF TYPE AND SCRAMBLED IT TO MAKE A TYPE SPECIMEN BOOK. IT HAS SURVIVED NOT ONLY FIVE CENTURIES,
                        BUT ALSO THE LEAP INTO ELECTRONIC TYPESETTING, REMAINING ESSENTIALLY UNCHANGED. IT WAS
                        POPULARISED IN THE 1960S WITH THE RELEASE OF LETRASET SHEETS CONTAINING LOREM IPSUM PASSAGES,
                        AND MORE RECENTLY WITH DESKTOP PUBLISHING SOFTWARE LIKE ALDUS PAGEMAKER INCLUDING VERSIONS OF
                        LOREM IPSULOREM IPSUM IS SIMPLY DUMMY TEXT OF THE PRINTING AND TYPESETTING INDUSTRY. LOREM IPSUM
                        HAS BEEN THE INDUSTRY'S STANDARD DUMMY TEXT EVER
                    </p>
                </div>
            </div>
            <div class="proprietes-produit my-4 col-lg-12">
                <div class="row">
                    <div class="col-lg-2 col-md-6">
                        <select name="style" id="style">
                            <option value="style">Style</option>
                        </select>
                    </div>
                    <div class="col-lg-2 col-md-6">
                        <select name="dimension" id="dimension">
                            <option value="Dimension">Dimension</option>
                        </select>
                    </div>
                    <div class="col-lg-2 col-md-6">
                        <select name="couleur" id="couleur">
                            <option value="Couleur">Couleur</option>
                        </select>
                    </div>
                    <div class="col-lg-2 col-md-6">
                        <select name="matiere" id="matiere">
                            <option value="Matière">Matière</option>
                        </select>
                    </div>
                    <div class="col-lg-2 col-md-6">
                        <select name="disponibilite" id="disponibilite">
                            <option value="Disponibilté">Disponibilté</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col-12 ">
                <div class="row">
                    <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 col-12 ">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 col-6">
                                <button class="btn btn-add">Ajouter au chariot</button>
                                <div class="col-12-lg quantite my-2">
                                    <div class=" row">
                                        <label
                                            class="col-lg-7 col-md-7 col-12 col-form-label label-qty"
                                            for="qty" style="font-size:11px;margin-top: 5px;">Choisissez
                                            la Quantité :</label>
                                        <div class="col-lg-5 col-md-5 col-12">
                                            <div class="row">
                                                <button class="btn btn-plus col-lg-3 col-md-3 col-sm-3 col-xs-3 col-3"
                                                    onclick="this.parentNode.querySelector('input[type=number]').stepUp()">+</button>
                                                <input class="col-lg-4 col-md-4 col-sm-4 col-xs-4 col-4" value="1"
                                                    min="1" type="number" id="qty" />
                                                <button class="btn btn-moins col-lg-3 col-md-3 col-sm-3 col-xs-3 col-3"
                                                    onclick="this.parentNode.querySelector('input[type=number]').stepDown()">-</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div  class=" addwishlist col-lg-6 col-md-6 col-sm-6 col-xs-6 col-6">
                                <button class="btn btn-add">Ajouter au favori <img class="icon" src="img/icon-heart.png"
                                        alt="favoris"></button>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-7 col-md-12 col-sm-12 col-xs-12 col-12 ">
                        <ul class="nav nav-tabs" role="tablist">
                            <li class="nav-item col-lg-4 col-md-6 col-sm-6 col-xs-6 col-6">
                                <a class="nav-link" data-toggle="tab" href="#creer-compte" role="tab">Créer un
                                    compte</a>
                            </li>
                            <li class="nav-item col-lg-4 col-md-6 col-sm-6 col-xs-6 col-6">
                                <a class="nav-link" data-toggle="tab" href="#s-identifier" role="tab">S'identifier</a>
                            </li>
                            <li class="nav-item col-lg-4 col-md-6 col-sm-6 col-xs-6 col-6">
                                <a class="nav-link  active" data-toggle="tab" href="#meme-categorie" role="tab">Même
                                    catégorie</a>
                            </li>
                        </ul><!-- Tab panes -->
                    </div>
                </div>
            </div>
        </div>
    </section>


    <section class="container-fluid my-4">
        <div class="tab-content">
            <div class="tab-pane" id="creer-compte" role="tabpanel">
                <?php include("inscription-form.php"); ?>
            </div>
            <div class="tab-pane" id="s-identifier" role="tabpanel">
                <?php include("login-form.php"); ?>
            </div>
            <div class="tab-pane active" id="meme-categorie" role="tabpanel">
                <section class="container-fluid">
                    <?php include('section-catégories.php') ?>
                </section>
            </div>
        </div>
    </section>
    <?php include('footer.php') ?>
    <?php include("back_to_top.php"); ?>
</body>

<script src="js/jQuery_v3_2_1.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/contact.js"></script>
     <script src="js/header.js"></script>
    <script src="js/carousel_categories.js"></script>
<script src="js/slider.js"></script>

</html>