$(document).ready(function() {
    $('#slider').owlCarousel({
        margin:30,
        loop:true,
        nav:true,
        dots: false,
        responsive:{
            0:{
                items:1
            },
            767:{
                items:1
            },
            1024:{
                items:1
            }
        }
    });
    });