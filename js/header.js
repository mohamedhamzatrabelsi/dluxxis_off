/**------------------------------------------------------------------------
 *                           SECTION SHOOPING CART
 *------------------------------------------------------------------------**/

$("#shooping-cart").hover(function(){
   $(".popup-cart").toggleClass("show");
})
$(".popup-cart").hover(function(){
  $(".content-cart").toggleClass("show");
})


/****mobile header*/

$(document).ready(function () {
   $('.menu-btn').click(function(event) {
       $('.navbar-demo').toggleClass('open-nav');
   });
});

$('#exampleModalCenter').on('hidden.bs.modal', function () {
   $("#qty_number").val(1)
});
// intial input quantity value in closing modal
const backToTop = document.querySelector("#back-to-top");
document.addEventListener("scroll", scrollFunction);
function scrollFunction(){
   if(window.pageYOffset > 300) {
       setTimeout(function(){
           backToTop.style.display = "block";
       }, 200);
   } else 
   {
       setTimeout(function(){
           backToTop.style.display = "none";
       }, 200);
   }
}
