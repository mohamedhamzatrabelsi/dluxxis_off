$(document).ready(function() {
$('.categories').owlCarousel({
    margin:30,
    loop:true,
    nav:true,
    dots: false,
    responsive:{
        0:{
            items:1
        },
        767:{
            items:2
        },
        1024:{
            items:3
        }
    }
});
});