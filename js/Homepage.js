/** add to wishlist */



$(".addwishlist").on("click", function() {
    $(this).toggleClass('added');
    // var selectedIds = $('.check_selected').map(function() {
    //     return this.id;
    // }).get();
    $('body').each(function(){
       var number_selected = $('.added').length;
       $('#counter-wishlist').empty();
       $('#counter-wishlist').append(number_selected);
    });
 });

/** */

/****** Carousel nos collections *****/
$('#nos-collections .owl-carousel').owlCarousel({
    loop: true,
    nav: true,
    responsiveClass: true,
    smartSpeed :1200,
    autoplay: true,
    autoplayTimeout: 5500,
    responsive: {
        0: {
            items: 1,
            nav: true,
            dots: false
        }
    },
    navText : ["<img src='img/prev.png'>", "<img src='img/next.png'>"]
  });
$('#nos-collections .owl-carousel .owl-nav button').on('mouseover', function(e) {
    $('.owl-carousel').trigger('stop.owl.autoplay');
});
$('#nos-collections .owl-carousel .owl-nav button').on('mouseleave', function(e) {
    $('.owl-carousel').trigger('play.owl.autoplay');
});


/****** Carousel meilleures collections *****/
$("#meilleures-collections .owl-carousel").owlCarousel({
loop: true,
margin:10,
smartSpeed :1200,
autoplay: true,
autoplayTimeout: 5500,
nav: true,
touchDrag: false,
mouseDrag: false,
navText : ["<img src='img/prev.png'>", "<img src='img/next.png'>"],
responsive:{
    0:{
        items:1
    },
    750:{
        items:2
    },
    1000:{
        items:3
    }
}
});
$('#meilleures-collections .owl-carousel .owl-nav button').on('mouseover', function(e) {
    $('.owl-carousel').trigger('stop.owl.autoplay');
});
$('#meilleures-collections .owl-carousel .owl-nav button').on('mouseleave', function(e) {
    $('.owl-carousel').trigger('play.owl.autoplay');
});

/****** Carousel nos articles bureautiques *****/
$("#articles-bureautiques .owl-carousel").owlCarousel({
loop: true,
margin:10,
smartSpeed :1200,
autoplay: true,
autoplayTimeout: 5500,
nav: true,
touchDrag: false,
mouseDrag: false,
navText : ["<img src='img/prev.png'>", "<img src='img/next.png'>"],
responsive:{
    0:{
        items:1
    },
    750:{
        items:2
    },
    1000:{
        items:3
    }
}
});
$('#articles-bureautiques .owl-carousel .owl-nav button').on('mouseover', function(e) {
    $('.owl-carousel').trigger('stop.owl.autoplay');
});
$('#articles-bureautiques .owl-carousel .owl-nav button').on('mouseleave', function(e) {
    $('.owl-carousel').trigger('play.owl.autoplay');
});

  var divisor = document.getElementById("divisor"),
    handle = document.getElementById("handle"),
    slider = document.getElementById("slider");
    divisor1 = document.getElementById("divisor1"),
    handle1 = document.getElementById("handle1"),
    slider1 = document.getElementById("slider1");
    divisor2 = document.getElementById("divisor2"),
    handle2 = document.getElementById("handle2"),
    slider2 = document.getElementById("slider2");
function moveDivisor() {
  handle.style.left = slider.value+"%";
	divisor.style.width = slider.value+"%";
}

window.onload = function() {
	moveDivisor();
};
function moveDivisor1() {
  handle1.style.left = slider1.value+"%";
	divisor1.style.width = slider1.value+"%";
}

window.onload = function() {
	moveDivisor1();
};
function moveDivisor2() {
  handle2.style.left = slider2.value+"%";
	divisor2.style.width = slider2.value+"%";
}

window.onload = function() {
	moveDivisor2();
};
$('.owl-carousel').owlCarousel({
    mouseDrag: false,
    margin:10,
    nav:true,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:3
        },
        1000:{
            items:3
        }
    }
})



//$("#meilleures-collections .image-item .box-content").click(function () {
    //var imgsrc = $(".img-modal").attr("src");
    //$("#img-preview").attr('src', '' + imgsrc + '');
    //$('#meilleures-collections .modal').modal({
       // show: true
    //});



//});






