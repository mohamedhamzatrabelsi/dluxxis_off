/**------------------------------------------------------------------------------------------------
 *                                         PHONE CODE COUNTRY SELECTOR
 *------------------------------------------------------------------------------------------------**/
$(document).ready(function () {
    $("[name='country']").on("change", function () {
        $("[name='phone']").val($(this).find("option:selected").data("code"));
    });
});
/**------------------------------------------------------------------------
 *                           FORM VALIDATION INSCRIPTION
 *------------------------------------------------------------------------**/
(function () {
    'use strict';
    window.addEventListener('load', function () {
        /**----------------------
         *    CONFIRMATION PASSWORD
         *------------------------**/
        $('#c_password').keyup(function () {
            if ($("#password").val() != $("#c_password").val()) {
                $("#c_password").css("border-color", "#dc3545");
                $("#cp").html('<span class="text-danger">Les mots de passe que vous avez entrés ne sont pas identiques.</span>');

            } else {
                $("#c_password").css("border-color", "green");
                $('.text-danger').remove();
            }
        });
        var form = document.getElementById('form-contact');
        if (form) {
            form.addEventListener('submit', function (event) {
                if (form.checkValidity() === false) {
                    event.preventDefault();
                    event.stopPropagation();                    
                }
                form.classList.add('was-validated');

                if ($("#phone").length != 12) {
                    $("#phone").css("border-color", "#dc3545");
                    $("#error-phone").html('<span class="text-danger">Veuillez saisir votre numéro de téléphone</span>')

                } else {
                    $("#phone").css("border-color", "green");
                    $("#error-phone").remove()
                }
                $('#phone').keyup(function () {
                    if ($(this).val().length != 12) {

                        $("#phone").css("border-color", "#dc3545");
                        $("#error-phone").html('<div class="text-danger">Veuillez saisir votre numéro de téléphone</div>')

                    } else {
                        $("#phone").css("border-color", "green");
                        $("#error-phone").remove();
                    }
                })

            }, false);
        }

    }, false);
})();

/**--------------------------------------------
 *               form validation mobile
 *---------------------------------------------**/
(function () {
    'use strict';
    window.addEventListener('load', function () {
        /**----------------------
         *    CONFIRMATION PASSWORD
         *------------------------**/
        $('#c_password_m').keyup(function () 
        {
            if ($("#password_m").val() != $("#c_password_m").val()) {
               
                $("#c_password_m").css("border-color", "#dc3545");
                $("#cp_m").html('<span class="text-danger">Les mots de passe que vous avez entrés ne sont pas identiques.</span>');

            } else {
                
                $("#c_password_m").css("border-color", "green");
                $('#cp_m').remove();
            }
        });
        var form = document.getElementById('form-contact2');
        if (form) {
            form.addEventListener('submit', function (event) {
                if (form.checkValidity() === false) {
                    event.preventDefault();
                    event.stopPropagation();
                }
                form.classList.add('was-validated');

                if ($("#phone_m").length != 12) {
                    $("#phone_m").css("border-color", "#dc3545");
                    $("#error-phone_m").html('<span class="text-danger">Veuillez saisir votre numéro de téléphone</span>')

                } else {
                    $("#phone_m").css("border-color", "green");
                    $("#error-phone_m").remove()
                }
                $('#phone_m').keyup(function () {
                    if ($(this).val().length != 12) {
                        $("#phone_m").css("border-color", "#dc3545");
                        $("#error-phone_m").html('<div class="text-danger">Veuillez saisir votre numéro de téléphone</div>')

                    } else {
                        $("#phone_m").css("border-color", "green");
                        $("#error-phone_m").remove();
                    }
                })

            }, false);
        }

    }, false);
})();

/**------------------------------------------------------------------------
 *                           LOGIN FORM VALIDATION
 *------------------------------------------------------------------------**/
(function () {
    'use strict';
    window.addEventListener('load', function () {       
        var form = document.getElementById('form-login');
        if (form) {
            form.addEventListener('submit', function (event) {
                if (form.checkValidity() === false) {
                    event.preventDefault();
                    event.stopPropagation();                    
                }
                form.classList.add('was-validated');

            }, false);
        }

    }, false);
})();

