<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/fiche-produit.css">
    <title>Document</title>
</head>
<body>
    <!--bloc service-->
<section id="Services">
            <div class="">
                <div class="row AllServices">
                    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 col-6">
                        <div class="livraison service_width text-center ">
                            <div class="groupe-image">
                            <img class="img-fluid" src="img/Groupe49.png" alt="">
                            </div>
                            <h3 class="h3_service">Livraison</h3>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 col-6">
                        <div class="paiement service_width text-center">
                        <div class="groupe-image">
                            <img class="img-fluid" src="img/Tracé173.png" alt="">
                        </div>
                            <h3 class="h3_service">Paiement sécurisé</h3>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 col-6">
                        <div class="suivi service_width text-center">
                        <div class="groupe-image">
                            <img class="img-fluid" src="img/Tracé177.png" alt="">
                        </div>
                            <h3 class="h3_service">Suivi de commande</h3>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 col-6">
                        <div class="suivi service_width text-center">
                        <div class="groupe-image">
                            <img class="img-fluid" src="img/Tracé175.png" alt="">
                        </div>
                            <h3 class="h3_service">Réclamation</h3>
                        </div>
                    </div>
                </div>
            </div>
        </section>
</body>
</html>