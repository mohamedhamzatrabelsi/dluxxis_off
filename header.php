

<div class="container-fluid">


    <div class="row">

        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 my-auto">
        <div class="row">
                <div class="pad_remove">
                    <div class="recherch_block">
                        <input class="my_input" type="search" placeholder="Rechercher" aria-label="Search">
                        <button class="btn my_btn_input" type="submit">
                            <img src="img/header_dluxxix/icon_view.png" alt="icon search">
                        </button>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 text-center">
            <a href="/dluxxis_off"><img id="header_logo" src="img/header_dluxxix/logo-dlluxxis.png" class="img-fluid" alt="logo dluxxis"></a>
        </div>

        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 my-auto">
            <div class="row">
                <div class="col-12 col-md-12 icons">
                <div class="row">
                <div class="col-6 col-md-6  pr-0 pl-0">
                <div class="dropdown btn_newsletter float-right">
                     <button class="btn btn_newsletter dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Newsletter
                      </button>
                 <div class="dropdown-menu toogle_newsletter" aria-labelledby="dropdownMenuButton">
                     <p class="newsletter_txt">
                   Inscrivez-vous à notre <br> Newsletter et recevez chaque <br> mois des offres <br> exclusives, actualités ...
                       </p>
                       <div class="newsletter ns_header">
                        <img src="img/newslettre.png" alt="icon newslettre">
                        <input type="email" placeholder="Votre email" name="email" required="required">
                        <button type="submit" class="bt-nl">OK</button>
                    </div>
     
                 </div>
                 </div>
                 </div>
                 <div class="col-6 col-md-6 padding_up ">
                    <div class="float-right pt-1">
                        <a href="signup.php"><img src="img/header_dluxxix/user-header.png" class="img_ic" alt="icon user"></a>

                        <a id="wishlist-a" href="#"><img src="img/header_dluxxix/wishlist-header.png" class="img_ic" alt="icon wishlist">
                        <span id="counter-wishlist">0</span>
                        </a>

                        <a id="shooping-cart" href="#"><img src="img/header_dluxxix/cart-header.png" class="img_ic" alt="icon shooping cart">
                            <div class="popup-cart">
                                <div class="content-cart">
                                <ul>
                                    <li class="row">
                                        <div class="img-cart-content col-lg-4">
                                                <img src="img/Rectangle_33.png" class="img-fluid" alt="image produit shooping cart">
                                                <span class="product-quantity">3x</span>
                                        </div>
                                        <div class="right-block col-lg-8 row">
                                                <span class="product-name col-lg-12">Nom de produit</span>
                                                <span class="product-price font-weight-bold col-lg-12">15 €</span>
                                        </div>
                                    </li>
                                    <li class="row">
                                        <div class="img-cart-content col-lg-4">
                                                <img src="img/Rectangle_33.png" class="img-fluid" alt="image produit shooping cart">
                                                <span class="product-quantity">3x</span>
                                        </div>
                                        <div class="right-block col-lg-8 row">
                                                <span class="product-name col-lg-12">Nom de produit</span>
                                                <span class="product-price font-weight-bold col-lg-12">15 €</span>
                                        </div>
                                    </li>
                                    <li class="row">
                                        <div class="img-cart-content col-lg-4">
                                                <img src="img/Rectangle_33.png" class="img-fluid" alt="image produit shooping cart">
                                                <span class="product-quantity">3x</span>
                                        </div>
                                        <div class="right-block col-lg-8 row">
                                                <span class="product-name col-lg-12">Nom de produit</span>
                                                <span class="product-price font-weight-bold col-lg-12">15 €</span>
                                        </div>
                                    </li>
                                </ul>
                                <div class="price-content row">
                                        <span class="label col-lg-6">Total</span>
                                        <span class="value col-lg-6">35 €</span>
                                </div>
                                <div class="checkout">
                                    <button href="#" class="btn btn-checkout col-lg-12">Commander</button>
                                </div>
                                </div>
                            </div>
                        </a>

                        <span class="dropdown">
                        <a href="#"><img src="img/header_dluxxix/message-header.png" class="img_ic" alt="icon commentaire"></a>
                        <span class="dropdown-content">
                        <p> LOREM IPSUM IS SIMPLY DUMMY TEXT OF THE PRINTING AND TYPESETTING INDUSTRY.
                           </p>
</span>
</span>
                        <a href="#"><img src="img/header_dluxxix/connexion-header.png" class="img_ic" alt="icon connexion"></a>
                    </div>
                    

                </div>
            </div>
        </div>
        </div>
        </div>
    </div>




</div>
<!-- header js -->


