<div class="mobile row">
        <div  id="header" class="navbar-demo">
	
<nav role="navigation">
  <div id="menuToggle">
    <input type="checkbox" />
    <span></span>
    <span></span>
    <span></span>
    <ul id="menu">
      <a href="#"><li>Accueil</li></a>
      <a href="#"><li>A propos</li></a>
      <a href="#"><li>Nos collections</li></a>
      <a href="#"><li>Meilleures collections</li></a>
      <a href="#"><li>Nos articles</li></a>
      <a href="#"><li>Nos articles bureautiques</li></a>
      <a href="#"><li>Blog</li></a>
      <a href="#"><li>Contact</li></a>
    </ul>
  </div>
</nav>
</div> 

<div class="text-center">
            <a href="/dluxxis_off"><img id="header_logo" src="img/header_dluxxix/logo-dlluxxis.png" class="img-fluid"></a>
        </div>

<div class="">
    <a id="shooping-cart" href="#"><img src="img/header_dluxxix/cart-header.png" class="img_ic">
                            <div class="popup-cart">
                                <div class="content-cart">
                                <ul>
                                    <li class="row">
                                        <div class="img-cart-content col-lg-4">
                                                <img src="img/Rectangle_33.png" class="img-fluid" alt="">
                                                <span class="product-quantity">3x</span>
                                        </div>
                                        <div class="right-block col-lg-8 row">
                                                <span class="product-name col-lg-12">Nom de produit</span>
                                                <span class="product-price font-weight-bold col-lg-12">15 €</span>
                                        </div>
                                    </li>
                                    <li class="row">
                                        <div class="img-cart-content col-lg-4">
                                                <img src="img/Rectangle_33.png" class="img-fluid" alt="">
                                                <span class="product-quantity">3x</span>
                                        </div>
                                        <div class="right-block col-lg-8 row">
                                                <span class="product-name col-lg-12">Nom de produit</span>
                                                <span class="product-price font-weight-bold col-lg-12">15 €</span>
                                        </div>
                                    </li>
                                    <li class="row">
                                        <div class="img-cart-content col-lg-4">
                                                <img src="img/Rectangle_33.png" class="img-fluid" alt="">
                                                <span class="product-quantity">3x</span>
                                        </div>
                                        <div class="right-block col-lg-8 row">
                                                <span class="product-name col-lg-12">Nom de produit</span>
                                                <span class="product-price font-weight-bold col-lg-12">15 €</span>
                                        </div>
                                    </li>
                                </ul>
                                <div class="price-content row">
                                        <span class="label col-lg-6">Total</span>
                                        <span class="value col-lg-6">35 €</span>
                                </div>
                                <div class="checkout">
                                    <button href="#" class="btn btn-checkout col-lg-12">Commander</button>
                                </div>
                                </div>
                            </div>
                        </a></div>
</div>