<!DOCTYPE html> 
<head>

    <!--style css -->
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">
    <link rel="stylesheet" href="css/bannerCarrousel.css">
    <link rel="stylesheet" href="css/Homepage.css">
    <link rel="stylesheet" href="css/nos_collections.css">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, minimum-scale=1">
    <title>dluxxis</title>

   
</head>

<body>
   
    <div class="heady">
         <?php include("header.php"); ?> 
    </div>

    <div class="header-mobile">
         <?php include("header-mobile.php"); ?> 
    </div>

    <section id="banner_carousel" class="pt-0">
        <?php include("bannerCarrousel.php"); ?>
    </section>

    <section id="nos_collection">
        <?php include("nos_collection.php"); ?>
    </section>

    <section id="meilleures_collection">
        <?php include("meilleures_collection.php"); ?>
    </section>

    <section id="nos_articles">
    <?php include("nos_article.php"); ?>
    </section>

    <section id="nos_articles_bureautique">
        <?php include("nos_articles_bureautique.php"); ?>
    </section> 

    <section id="blog">
        <?php include("section_blog.php"); ?>
    </section> 
    
         <?php include("footer.php"); ?>

         <?php include("back_to_top.php"); ?>
           <!-- JS home page -->
    
    

    <!-- JS contact page -->

    <script src="js/jQuery_v3_2_1.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/contact.js"></script>
     <script src="js/header.js"></script>
    <script src="js/Homepage.js"></script>
    <script src="js/carousel_categories.js"></script>



</body>
   
</html>
