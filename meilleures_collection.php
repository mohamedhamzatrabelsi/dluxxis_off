<section id="meilleures-collections">
    <div class="container-fluid">
        <h2 class="h2_class">Meilleures collections</h2>
        <div class="owl-carousel">
            <div>
                <div class="image-item">
                    <img src="img/chambre-parent.png" alt="">
                    <div class="box-content">
                        <ul class="icon search-anim">
                            <li><a data-toggle="modal" data-target="#exampleModalLong1">
                                <img src="img/header_dluxxix/icon_view.png" alt="quick_view"></a></li>

                        </ul>
                    </div>

                </div>
                <div class="div_mc">
                    <p class="p_meilleures_collection">Chambre à coucher</p>
                </div>
            </div>
            <div>
                <div class="image-item">
                    <img src="img/salle-a-manger.png" alt="">
                    <div class="box-content">
                        <ul class="icon search-anim">
                            <li><a data-toggle="modal" data-target="#exampleModalLong2">
                                <img src="img/header_dluxxix/icon_view.png" alt="quick_view" ></a> </li>
                        </ul>
                    </div>

                </div>
                <div class="div_mc">
                    <p class="p_meilleures_collection">Salon</p>
                </div>
            </div>
            <div>
                <div class="image-item">
                    <img src="img/meuble-dentree.png" alt="" class="img-fluid img-modal">
                    <div class="box-content">
                        <ul class="icon search-anim">
                            <li><a data-toggle="modal" data-target="#exampleModalLong3"><img src="img/header_dluxxix/icon_view.png" alt="quick_view"></a> </li>

                        </ul>
                    </div>

                </div>
                <div class="div_mc">
                    <p class="p_meilleures_collection">Meuble d'entrée</p>
                </div>

            </div>
            <div>
                <div class="image-item">
                    <img src="img/chambre-parent.png" alt="" class="img-fluid img-modal">
                    <div class="box-content">
                        <ul class="icon search-anim">
                            <li><a data-toggle="modal" data-target="#exampleModalLong1"><img src="img/header_dluxxix/icon_view.png" alt="quick_view"></a> </li>

                        </ul>
                    </div>

                </div>
                <div class="div_mc">
                    <p class="p_meilleures_collection">Chambre à coucher</p>
                </div>
            </div>
            <div>
                <div class="image-item">
                    <img src="img/salle-a-manger.png" alt="" class="img-fluid img-modal">
                    <div class="box-content">
                        <ul class="icon search-anim">
                            <li><a data-toggle="modal" data-target="#exampleModalLong2"><img src="img/header_dluxxix/icon_view.png" alt="quick_view"></a> </li>

                        </ul>
                    </div>

                </div>

                <div class="div_mc">
                    <p class="p_meilleures_collection">Salon</p>
                </div>
            </div>
            <div>
                <div class="image-item">
                    <img src="img/meuble-dentree.png" alt="" class="img-fluid img-modal">
                    <div class="box-content search-anim">
                        <ul class="icon">
                            <li><a data-toggle="modal" data-target="#exampleModalLong3"><img src="img/header_dluxxix/icon_view.png" alt="quick_view"></a> </li>

                        </ul>
                    </div>

                </div>
                <div class="div_mc">
                    <p class="p_meilleures_collection">Meuble d'entrée</p>
                </div>
            </div>
            <div>
                <div class="image-item">
                    <img src="img/salle-a-manger.png" alt="" class="img-fluid img-modal">
                    <div class="box-content search-anim">
                        <ul class="icon">
                            <li><a data-toggle="modal" data-target="#exampleModalLong2"><img src="img/header_dluxxix/icon_view.png" alt="quick_view"></a> </li>

                        </ul>
                    </div>

                </div>
                <div class="div_mc">
                    <p class="p_meilleures_collection">Salon</p>
                </div>
            </div>
        </div>

    </div>


   

    <!-- Modal1 -->
    <div class="modal fade" id="exampleModalLong1" tabindex="1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <img class="" src="img/chambre-parent.png" alt="">
      </div>
    </div>
  </div>


   <!-- Modal2 -->
   <div class="modal fade" id="exampleModalLong2" tabindex="1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <img class="" src="img/salle-a-manger.png" alt="">
      </div>
    </div>
  </div>


  <!-- Modal2 -->
  <div class="modal fade" id="exampleModalLong3" tabindex="1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <img class="" src="img/meuble-dentree.png" alt="">
      </div>
    </div>
  </div>
</section>