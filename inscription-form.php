<div class="container">
    <h1 class="titre-connexion-espace">Créer rapidement <span class="brown-text">votre compte</span></h1>
    <div id="inscription-form" class="col-10 col-md-6 cnx-box mx-auto">
        <form id="form-contact"  action="" method="post" novalidate>
            <div class="form-group row">
                <label for="email" class="col-lg-12 col-md-12 col-sm-12 col-12 text-center">Adresse email*</label>
                <div class="mx-auto col-lg-10 col-md-10 col-sm-12 col-12">
                    <input class="form-control col-lg-12 col-md-12 col-sm-12 col-12" type="email" name="email" id="email" required>
                    <div class="invalid-feedback text-center">Veuillez saisir votre adresse e-mail</div>                    
                </div>
            </div>
            <div class="form-group row">
                <label for="password" class="col-lg-12 col-md-12 col-sm-12 col-12 text-center">Mot de passe*</label>
                <div class="mx-auto col-lg-10 col-md-10 col-sm-12 col-12">
                    <input class="form-control col-lg-12 col-md-12 col-sm-12 col-12" type="password" name="password" autocomplete id="password" required>
                    <div class="invalid-feedback text-center">Veuillez saisir votre mot de passe</div>                    
                </div>
            </div>
            <div class="form-group row">
                <label for="c_password" class="col-lg-12 col-md-12 col-sm-12 col-12 text-center">Confirmation mot de
                    passe*</label>
                    <div class="mx-auto col-lg-10 col-md-10 col-sm-12 col-12">
                        <input class="form-control col-lg-12 col-md-12 col-sm-12 col-12" type="password" name="password" autocomplete id="c_password" required>
                        <div class="invalid-feedback text-center">Veuillez confirmer votre mot de passe</div>
                        <em id="cp_m"></em>                        
                    </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-12 form-check ">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-12 form-check-inline">
                        <input class="form-check-input" type="checkbox" id="newsletter" value="option1">
                        <label class="form-check-label" for="newsletter">Jesouhaiterecevoir la
                            newsletter</label>
                    </div>
                </div>
                <div class="col-lg-12 form-check ">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-12 form-check-inline">
                        <input class="form-check-input " type="checkbox" id="conditiongenerales" value="option1" required>
                        <span class="custom-control-indicator"></span>
                        <label class="form-check-label pt-3" for="conditiongenerales">J’accepte les conditions
                            générales et la politique deconfidentialité</label>
                    </div>
                </div>
            </div>
    </div>
    <div class="btn-submit-container col-10 col-md-4 mx-auto">
        <div class="form-group row">
            <button type="submit" class="btn btn-submit">Valider </button>
        </div>
    </div>
    </form>
</div>