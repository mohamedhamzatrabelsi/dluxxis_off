
<div class="container-fluid">
<h2 class="h2_class">Nos articles</h2>
<div class="owl-carousel owl-theme">
        <div id="comparison">
            <div class="figure1">
              <div id="handle"></div>
              <div id="divisor"></div>
            </div>
            <input type="range" min="0" max="100" value="8" id="slider" oninput="moveDivisor()">
          </div>
          <div id="comparison">
            <div class="figure2">
              <div id="handle1"></div>
              <div id="divisor1"></div>
            </div>
            <input type="range" min="0" max="100" value="8" id="slider1" oninput="moveDivisor1()">
          </div>
          <div id="comparison">
            <div class="figure3">
              <div id="handle2"></div>
              <div id="divisor2"></div>
            </div>
            <input type="range" min="0" max="100" value="8" id="slider2" oninput="moveDivisor2()">
          </div>
        </div>
    </div>
