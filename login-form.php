<div class="container">
    <h1 class="titre-connexion-espace">Connectez vous <span class="brown-text">à votre compte</span></h1>
    <div  class="col-10 col-md-6 cnx-box mx-auto">
        <form id="form-login" action="" method="post" novalidate>
            <div class="form-group row">
                <label for="email_login" class="col-lg-12 col-md-12 col-sm-12 col-12 text-center">Adresse email</label>
                <div class="mx-auto col-lg-10 col-md-10 col-sm-12 col-12">
                    <input class="form-control col-lg-12 col-md-12 col-sm-12 col-12" type="email" name="email" id="email_login" required>
                    <div class="invalid-feedback text-center">Veuillez saisir votre email</div>
                </div>
            </div>
            <div class="form-group row">
                <label for="password_login" class="col-lg-12 col-md-12 col-sm-12 col-12 text-center">Mot de
                    passe</label>
                <div class="mx-auto col-lg-10 col-md-10 col-sm-12 col-12">
                    <input class="form-control col-lg-12 col-md-12 col-sm-12 col-12" type="password" name="password" id="password_login"
                        autocomplete required>
                        <div class="invalid-feedback text-center">Veuillez saisir votre mot de passe</div>                    
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-12 text-center">
                <a href="#" class="reset-password">Mot de passe oublié ?</a>
            </div>
    </div>
    <div class="btn-submit-container col-10 col-md-4 mx-auto">
        <div class="form-group row">
            <button type="submit" class="btn btn-submit">Se connecter</button>
        </div>
    </div>
    </form>
</div>