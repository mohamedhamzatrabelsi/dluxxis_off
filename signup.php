<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, minimum-scale=1">
    <title>Inscription</title>
    <link rel="stylesheet" href="css/signup.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/bannerCarrousel.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">

</head>

<body>
    <div class="heady">
        <?php include("header.php"); ?>
    </div>
    <div class="header-mobile">
        <?php include("header-mobile.php"); ?>
    </div>
    <section id="banner_carousel">
        <?php include("bannerCarrousel.php"); ?>
    </section>
    <section id="section-contact" class="container-fluid">
        <form id="form-contact" action="" method="post" novalidate>
            <h1>Création d’un compte client</h1>
            <div class="header-form">
                <div class="row">
                    <div class="col-lg-4">
                        <h2>Mon compte</h2>
                    </div>
                    <div class="col-lg-4">
                        <h2>Mes informations personnelles </h2>
                    </div>
                    <div class="col-lg-4">
                        <h2>Mon adresse de Facturation</h2>
                    </div>
                </div>
            </div>
            <div class="contact-body">
                <div class="row">
                    <div class="col-lg-4 ">
                        <div class="col-lg-12 form-group row">
                            <label for="email" class="col-lg-4 col-form-label">Adresse email* </label>
                            <div class="col-lg-8">
                                <input type="email" class="form-control" name="email" id="email" required>
                                <div class="invalid-feedback">Veuillez saisir votre adresse e-mail</div>
                            </div>
                        </div>
                        <div class="col-lg-12 form-group row">
                            <label for="password" class="col-lg-4 col-form-label">Mot de passe* </label>
                            <div class="col-lg-8">
                                <input type="password" class="form-control" name="password" id="password"
                                    autocomplete="on" required>
                                    <div class="invalid-feedback">Veuillez saisir votre mot de passe</div>
                            </div>
                        </div>
                        <div class="col-lg-12 form-group row">
                            <label for="c_password" class="col-lg-4 col-form-label">Confirmation mot de passe* </label>
                            <div class="col-lg-8">
                                <input type="password" class="form-control" name="c_password" id="c_password"
                                    autocomplete="on" required>
                                    <div class="invalid-feedback">Veuillez confirmer votre mot de passe</div>
                                    <em id="cp"></em>
                            </div>
                        </div>
                        <div class="col-lg-12 form-check ">
                            <div class="offset-lg-3 form-check-inline">
                                <input class="form-check-input" type="checkbox" id="newsletter" value="option1">
                                <label class="form-check-label" for="newsletter">Jesouhaiterecevoir la
                                    newsletter</label>
                            </div>
                        </div>
                        <div class="col-lg-12 form-check ">
                            <div class="offset-lg-3 form-check-inline">
                                <input class="form-check-input" type="checkbox" id="conditiongenerales" value="option1" required>
                                <span class="custom-control-indicator"></span>
                                <label class="form-check-label pt-3" for="conditiongenerales">J’accepte les conditions
                                    générales et la politique deconfidentialité</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 ">
                        <div class="col-lg-12 form-group row">
                            <label class="d-inline-block col-form-label  col-lg-4" for="civilite">Civilité* </label>
                            <div class="col-lg-8">
                                <select class="custom-select " name="civilite" id="civilite" required>
                                    <option value="">choisez votre civilité</option>
                                    <option value="m" >Mr</option>
                                    <option value="mme">Mme</option>
                                </select>
                                <div class="invalid-feedback">Veuillez choisir votre civilité</div>
                            </div>
                        </div>
                        <div class="col-lg-12 form-group row">
                            <label for="nom" class="col-lg-4 col-form-label">Nom*</label>
                            <div class="col-lg-8">
                                <input type="text" class="form-control" name="nom" id="nom" required>
                                <div class="invalid-feedback">Veuillez saisir votre nom</div>
                            </div>
                        </div>
                        <div class="col-lg-12 form-group row">
                            <label for="prenom" class="col-lg-4 col-form-label">Prénom*</label>
                            <div class="col-lg-8">
                                <input type="text" class="form-control" name="prenom" id="prenom" required>
                                <div class="invalid-feedback">Veuillez saisir votre prénom</div>
                            </div>
                        </div>
                        <div class="col-lg-12 form-group row">
                            <label for="date_naissance" class="col-lg-4 col-form-label">Date de naissance</label>
                            <div class="col-lg-8">
                                <input type="date" class="form-control" name="date_naissance" id="date_naissance">
                            </div>
                        </div>
                        <div class="col-lg-12 form-group row">
                            <label for="prenom" class="col-lg-8 col-form-label">Comment avez-vous connu
                                D’luxxis?</label>
                            <div class="col-lg-12">
                                <select class="custom-select col-lg-8">
                                    <option value="press" selected>Press</option>
                                    <option value="boutique">Boutique</option>
                                    <option value="google">Google</option>
                                    <option value="affiche_metro">Affiche métro</option>
                                    <option value="flyer">Flyer</option>
                                    <option value="bouche_a_oreille">Bouche à oreille</option>
                                    <option value="pubTv">PubTV</option>
                                    <option value="radio">Radio</option>
                                    <option value="prescripteur">Prescripteur</option>
                                    <option value="autre">Autre</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 ">
                        <div class="form-group col-lg-12">
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="nature_compte" value="particulier" id="particulier"
                                    value="particulier" checked>
                                <label class="form-check-label" for="particulier">
                                    Particulier
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="nature_compte" value="professionnel" id="professionnel"
                                    value="professionnel">
                                <label class="form-check-label" for="professionnel">
                                    Professionnel
                                </label>
                            </div>
                        </div>
                        <div class="col-lg-12 form-group row">
                            <label for="adresse" class="col-lg-4 col-form-label">Adresse*</label>
                            <div class="col-lg-8">
                                <textarea name="adresse" id="adresse" class="form-control" rows="3" required></textarea>
                                <div class="invalid-feedback">Veuillez saisir votre adresse</div>
                            </div>
                        </div>
                        <div class="col-lg-12 form-group row">
                            <label for="code_postal" class="col-lg-4 col-form-label">Code Postal*</label>
                            <div class="col-lg-8">
                                <input type="text" class="form-control" name="code_postal" id="code_postal" required>
                                <div class="invalid-feedback">Veuillez saisir votre code postal</div>
                            </div>
                        </div>
                        <div class="col-lg-12 form-group row">
                            <label for="ville" class="col-lg-4 col-form-label">Ville*</label>
                            <div class="col-lg-8">
                                <input type="text" class="form-control" name="ville" id="ville" required>
                                <div class="invalid-feedback">Veuillez saisir votre ville</div>
                            </div>
                        </div>
                        <div class="col-lg-12 form-group row">
                            <label for="pays" class="col-lg-4 col-form-label">Pays*</label>
                            <div class="col-lg-8">
                                <select class="custom-select col-lg-12" name="pays" id="pays" required>
                                    <option value="">choisez votre pays</option>
                                    <option value="france" >France</option>
                                    <option value="allemagne">Allemagne</option>
                                </select>
                                <div class="invalid-feedback">Veuillez choisir votre pays</div>
                            </div>
                        </div>
                        <div class=" col-lg-12 form-group row">
                            <label for="pays" class="col-lg-4 col-form-label">Téléphone*</label>
                            <div class="col-lg-8 input-group input-group-sm">
                                <select name="country" class=" custom-select col-lg-4">
                                    <option data-code="+33" value="uk">Fr +33</option>
                                    <option data-code="+49" value="uk">Al +49</option>
                                </select>
                                <input id="phone" name="phone" type="tel" pattern="^[0-9-+\s()]*$" value="+33" min="11" max="11" class="form-control col-lg-8" required>
                                <div id="error-phone"></div>
                            </div>
                            <span class="info-tel offset-lg-1">
                                Nécessaire pour la prise derendez-vous lors la livraison
                            </span>
                        </div>
                    </div>
                </div>
                <div class="conact-footer mt-5">
                    <div class="row">
                        <div class="col-lg-6 text-right">
                            <button type="submit" class="btn btn-submit">Valider mon inscription</button>
                        </div>
                        <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12  text-right">
                            <div class="rs_div">
                                <a href="#"><img src="img/footer_dluxxix/facebook.png" class="img_soc"  alt="facebook"></a>
                        
                                <a href="#"><img src="img/footer_dluxxix/linkedin.png"  class="img_soc"  alt="linkedin"></a>
                        
                                <a href="#"><img src="img/footer_dluxxix/instagram.png"  class="img_soc"  alt="instagram"></a>
                        
                                <a href="#"><img src="img/footer_dluxxix/printerest.png" class="img_soc"  alt="printerest" ></a>
                        
                                <a href="#"><img src="img/footer_dluxxix/twitter.png" class="img_soc" alt="twitter"></a>
                            </div>
                        </div>
                    </div>
                </div>
        </form>
    </section>
    <section id="section-contact-mobile" class="container-fluid">
        <form id="form-contact2" action="" method="post" novalidate>
            <h1>Création d’un compte client</h1>
            <div class="header-form">
                <div class="row">
                    <div class="col-lg-4 col-12">
                        <h2>Mon compte</h2>
                        <div class="col-lg-4 col-12">
                            <div class="col-lg-12 form-group row">
                                <label for="email" class="col-lg-4 col-12 col-form-label">Adresse email* </label>
                                <div class="col-lg-8 col-12">
                                    <input type="email" class="form-control" name="email" id="email-m" required>
                                    <div class="invalid-feedback">Veuillez saisir votre adresse e-mail</div>
                                </div>
                            </div>
                            <div class="col-lg-12 form-group row">
                                <label for="password_m" class="col-lg-4 col-12 col-form-label">Mot de passe* </label>
                                <div class="col-lg-8 col-12">
                                    <input type="password" class="form-control" name="password" id="password_m"
                                        autocomplete="on" required>
                                        <div class="invalid-feedback">Veuillez saisir votre mot de passe</div>
                                </div>
                            </div>
                            <div class="col-lg-12 form-group row">
                                <label for="c_password_m" class="col-lg-4 col-12 col-form-label">Confirmation mot de
                                    passe* </label>
                                <div class="col-lg-8 col-12">
                                    <input type="password" class="form-control" name="c_password" id="c_password_m"
                                        autocomplete="on" required>
                                        <div class="invalid-feedback">Veuillez confirmer votre mot de passe</div>
                                    <em id="cp_m"></em>
                                </div>
                            </div>
                            <div class="col-lg-12 form-check ">
                                <div class="offset-lg-3 form-check-inline">
                                    <input class="form-check-input" type="checkbox" id="newsletter-m" value="option1">
                                    <label class="form-check-label" for="newsletter">Jesouhaiterecevoir la
                                        newsletter</label>
                                </div>
                            </div>
                            <div class="col-lg-12 form-check ">
                                <div class="offset-lg-3 form-check-inline">
                                    <input class="form-check-input" type="checkbox" id="conditiongenerales-m"
                                        value="option1" required>
                                        <span class="custom-control-indicator"></span>
                                    <label class="form-check-label pt-3" for="conditiongenerales">J’accepte les
                                        conditions
                                        générales et la politique deconfidentialité</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-12">
                        <h2 class="h2_mobile">Mes informations personnelles </h2>
                        <div class="col-lg-4 col-12">
                            <div class="form-group form-inline">
                                <label class="d-inline-block col-form-label  col-lg-3" for="civilite">Civilité* </label>
                                <select class="custom-select col-lg-3" name="civilite" id="civilite_m" required>
                                    <option value="">choisez votre civilité</option>
                                    <option value="m" >Mr</option>
                                    <option value="mme">Mme</option>
                                </select>
                                <div class="invalid-feedback">Veuillez choisir votre civilité</div>
                            </div>
                            <div class="col-lg-12 form-group row">
                                <label for="nom" class="col-lg-4 col-12 col-form-label">Nom*</label>
                                <div class="col-lg-8">
                                    <input type="text" class="form-control" name="nom" id="nom-m" required>
                                    <div class="invalid-feedback">Veuillez saisir votre nom</div>
                                </div>
                            </div>
                            <div class="col-lg-12 form-group row">
                                <label for="prenom" class="col-lg-4 col-12 col-form-label">Prénom*</label>
                                <div class="col-lg-8">
                                    <input type="text" class="form-control" name="prenom" id="prenom-m" required>
                                    <div class="invalid-feedback">Veuillez saisir votre prénom</div>
                                </div>
                            </div>
                            <div class="col-lg-12 form-group row">
                                <label for="date_naissance" class="col-lg-4 col-12 col-form-label">Date de
                                    naissance</label>
                                <div class="col-lg-8">
                                    <input type="date" class="form-control" name="date_naissance" id="date_naissance-m">
                                </div>
                            </div>
                            <div class="col-lg-12 form-group row">
                                <label for="prenom" class="col-lg-8 col-12 col-form-label">Comment avez-vous connu
                                    D’luxxis?</label>
                                <div class="col-lg-12">
                                    <select class="custom-select col-lg-8 col-12">
                                        <option value="press" selected>Press</option>
                                        <option value="boutique">Boutique</option>
                                        <option value="google">Google</option>
                                        <option value="affiche_metro">Affiche métro</option>
                                        <option value="flyer">Flyer</option>
                                        <option value="bouche_a_oreille">Bouche à oreille</option>
                                        <option value="pubTv">PubTV</option>
                                        <option value="radio">Radio</option>
                                        <option value="prescripteur">Prescripteur</option>
                                        <option value="autre">Autre</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-12">
                        <h2 class="h2_mobile">Mon adresse de Facturation</h2>
                        <div class="col-lg-4 col-12">
                            <div class="form-group col-lg-12">
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="particulier" id="particulier-m"
                                        value="particulier" checked>
                                    <label class="form-check-label" for="particulier">
                                        Particulier
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="professionnel"
                                        id="professionnel-m" value="professionnel">
                                    <label class="form-check-label" for="professionnel">
                                        Professionnel
                                    </label>
                                </div>
                            </div>
                            <div class="col-lg-12 form-group row">
                                <label for="adresse" class="col-lg-4 col-12 col-form-label">Adresse*</label>
                                <div class="col-lg-8 col-12">
                                    <textarea name="adresse" id="adresse-m" class="form-control" rows="3" required></textarea>
                                    <div class="invalid-feedback">Veuillez saisir votre adresse</div>
                                </div>
                            </div>
                            <div class="col-lg-12 form-group row">
                                <label for="code_postal" class="col-lg-4 col-12 col-form-label">Code Postal*</label>
                                <div class="col-lg-8 col-12">
                                    <input type="text" class="form-control" name="code_postal" id="code_postal-m" required>
                                    <div class="invalid-feedback">Veuillez saisir votre code postal</div>
                                </div>
                            </div>
                            <div class="col-lg-12 form-group row">
                                <label for="ville" class="col-lg-4 col-12 col-form-label">Ville*</label>
                                <div class="col-lg-8 col-12">
                                    <input type="text" class="form-control" name="ville" id="ville-m" required>
                                    <div class="invalid-feedback">Veuillez saisir votre ville</div>
                                </div>
                            </div>
                            <div class="col-lg-12 form-group row">
                                <label for="pays" class="col-lg-4 col-12 col-form-label">Pays*</label>
                                <div class="col-lg-8 col-12">
                                    <select class="custom-select col-lg-12 col-12"  name="pays" id="pays_m" required>
                                        <option value="">choisez votre pays</option>
                                        <option value="france" >France</option>
                                        <option value="allemagne">Allemagne</option>
                                    </select>
                                </div>
                            </div>
                            <div class=" col-lg-12 form-group row">
                                <label for="pays" class="col-lg-4 col-12 col-form-label">Téléphone*</label>
                                <div class="col-lg-8 input-group input-group-sm">
                                    <select name="country" class=" custom-select col-lg-4 col-4">
                                        <option data-code="+33" value="uk">Fr +33</option>
                                        <option data-code="+49" value="uk">Al +49</option>
                                    </select>
                                    <input id="phone_m" name="phone" type="phone" value="+33" min="11" max="11" pattern="^[0-9-+\s()]*$"
                                        class="form-control col-lg-8  col-8">
                                        <div id="error-phone_m"></div>
                                </div>
                                <span class="info-tel offset-lg-1 col-12">
                                    Nécessaire pour la prise derendez-vous lors la livraison
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="conact-footer">
                    <div class="row">
                        <div class="col-sm-12 col-xs-12 text-center">
                            <button type="submit" class="btn btn-submit btn-sm">Valider mon inscription</button>
                        </div>
                        <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12  ">
                            <div class="rs_div_contact mx-4">
                            <a href="#"><img src="img/footer_dluxxix/facebook.png" class="img_soc"  alt="facebook"></a>
                        
                        <a href="#"><img src="img/footer_dluxxix/linkedin.png"  class="img_soc"  alt="linkedin"></a>
                
                        <a href="#"><img src="img/footer_dluxxix/instagram.png"  class="img_soc"  alt="instagram"></a>
                
                        <a href="#"><img src="img/footer_dluxxix/printerest.png" class="img_soc"  alt="printerest" ></a>
                
                        <a href="#"><img src="img/footer_dluxxix/twitter.png" class="img_soc" alt="twitter"></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </section>
    <?php include("footer.php"); ?>
    <?php include("back_to_top.php"); ?>
    <!-- script js -->
<script src="js/jQuery_v3_2_1.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/contact.js"></script>
     <script src="js/header.js"></script>


</body>

</html>