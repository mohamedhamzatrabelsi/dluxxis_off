<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, minimum-scale=1">
    <title>Contact</title>
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/bannerCarrousel.css">
    <link rel="stylesheet" href="css/contact.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">
    

</head>

<body>
    <div class="heady">
        <?php include("header.php"); ?>
    </div>
    <div class="header-mobile">
        <?php include("header-mobile.php"); ?>
    </div>
    <section id="banner_carousel">
        <?php include("bannerCarrousel.php"); ?>
    </section>
    <section id="section-contact" class="container-fluid mb-5">        
            <div class="container">
                <h1 class="titre-connexion-espace">Contactez <span class="brown-text">Nous</span></h1>
                <div class="row">
                    <div id="contact" class="col-lg-6 col-12 col-md-12 cnx-box mx-auto">
                    <form id="form-contact" action="" method="post" novalidate>
                            <div class="form-group row">
                                <label for="objet" class="col-lg-5 col-md-5 col-sm-12 col-12 text-left">Objet*</label>
                                <div class="col-lg-7 col-md-7 col-sm-12 col-12">
                                    <input class="form-control col-lg-12 col-md-12 col-sm-12 col-12" type="text" name="objet"
                                        id="objet" required>
                                        <div class="invalid-feedback">Veuillez saisir un objet</div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="nom" class="col-lg-5 col-md-5 col-sm-12 col-12 text-left">Nom*</label>
                                <div class="col-lg-7 col-md-7 col-sm-12 col-12">
                                    <input class="form-control col-lg-12 col-md-12 col-sm-12 col-12" type="text" name="nom" id="nom" required>
                                    <div class="invalid-feedback">Veuillez saisir votre nom</div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="prenom" class="col-lg-5 col-md-5 col-sm-12 col-12 text-left">Prénom*</label>
                                <div class="col-lg-7 col-md-7 col-sm-12 col-12">
                                    <input  class="form-control col-lg-12 col-md-12 col-sm-12 col-12" type="text" name="prenom" id="prenom" required>
                                        <div class="invalid-feedback">Veuillez saisir votre prénom</div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="email" class="col-lg-5 col-md-5 col-sm-12 col-12 text-left">Email*</label>
                                <div class="col-lg-7 col-md-7 col-sm-12 col-12">
                                    <input class="form-control col-lg-12 col-md-12 col-sm-12 col-12" type="email" name="email"
                                        id="email" required>
                                        <div class="invalid-feedback">Veuillez saisir votre e-mail</div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="email" class="col-lg-5 col-md-5 col-sm-12 col-12 text-left">Message*</label>
                                <div class="col-lg-7 col-md-7 col-sm-12 col-12">
                                    <textarea class="form-control col-lg-12 col-md-12 col-sm-12 col-12"  name="email"
                                        id="message" required></textarea>
                                        <div class="invalid-feedback">Veuillez saisir votre méssage</div>
                                </div>
                            </div>
                            <div class="btn-submit-container col-10 col-md-8 mx-auto">
                                <div  class="btn-container form-group row">
                                    <button type="submit" class="btn btn-submit">Envoyer</button>
                                </div>
                            </div>
                    </div>
                    <div class="col-lg-6 col-md-12 col-12 iframe-container">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3193.882862083163!2d10.187039215290518!3d36.821326779944236!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x12fd348b8536707f%3A0x249fdbb540618d9c!2sAvenue%20Kheireddine%20Pacha%2C%20Tunis!5e0!3m2!1sfr!2stn!4v1612961874461!5m2!1sfr!2stn" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                    </div>
                </div>
                
            </form>
        </div>        
    </section>

    <?php include("footer.php"); ?>
    <?php include("back_to_top.php"); ?>

<!-- script js -->
<script src="js/jQuery_v3_2_1.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/contact.js"></script>
     <script src="js/header.js"></script>
</body>

</html>