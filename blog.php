<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, minimum-scale=1">
    <title>blog</title>

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, minimum-scale=1">
    <title>Contact</title>
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/bannerCarrousel.css">
    <link rel="stylesheet" href="css/blog.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">

</head>

<body>
    <div class="heady">
        <?php include("header.php"); ?>
    </div>
    <div class="container-fluid" id="slider-blog1">
        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
                <div class="carousel-item active ">
                    <div class="carrousel-img">
                        <div class="col-lg-7 col-md-7 col-sm-12">
                            <img class="mw-100 image-container " src="img\img-blog 1.png" alt="" />
                        </div>
                        <div class="col-lg-5 col-md-5 col-sm-12 pt-3 description-bloc">
                            <span class="pl-3 span-blog">
                                LOREM IPSUM IS SIMPLY DUMMY TEXT OF THE PRINTING AND TYPESETTING INDUSTRY. LOREM IPSUM
                                HAS BEEN THE INDUSTRY'S STANDARD DUMMY TEXT EVER SINCE THE 1500S, WHEN AN UNKNOWN
                                PRINTER TOOK A GALLEY OF TYPE AND SCRAMBLED IT TO MAKE A TYPE SPECIMEN BOOK. IT HAS
                                SURVIVED NOT ONLY FIVE CENTURIES, BUT ALSO THE LEAP INTO ELECTRONIC TYPESETTING,
                                REMAINING ESSENTIALLY UNCHANGED. IT WAS POPULARISED IN THE 1960S WITH THE RELEASE OF
                                LETRASET SHEETS CONTAINING LOREM IPSUM PASSAGES, AND MORE RECENTLY WITH DESKTOP
                                PUBLISHING SOFTWARE LIKE ALDUS PAGEMAKER INCLUDING VERSIONS OF LOREM IPSULOREM IPSUM IS
                                SIMPLY DUMMY TEXT OF THE PRINTING AND TYPESETTING INDUSTRY. LOREM IPSUM HAS BEEN THE
                                INDUSTRY'S STANDARD DUMMY TEXT EVERLOREM IPSUM IS SIMPLY DUMMY TEXT OF THE PRINTING AND
                                TYPESETTING INDUSTRY. LOREM IPSUM HAS BEEN THE INDUSTRY'S STANDARD DUMMY TEXT EVER SINCE
                                THE 1500S, WHEN AN UNKNOWN PRINTER TOOK A GALLEY OF TYPE AND SCRAMBLED IT TO MAKE A TYPE
                                SPECIMEN BOOK. IT HAS SURVIVED NOT ONLY FIVE CENTURIES, BUT ALSO THE LEAP INTO
                                ELECTRONIC TYPESETTING, REMAINING ESSENTIALLY UNCHANGED. IT WAS POPULARISED IN THE 1960S
                                WITH THE RELEASE OF LETRASET SHEETS CONTAINING LOREM IPSUM PASSAGES, AND MORE RECENTLY
                                WITH DESKTOP PUBLISHING SOFTWARE LIKE ALDUS PAGEMAKER INCLUDING VERSIONS OF LOREM
                                IPSULOREM IPSUM IS SIMPLY DUMMY TEXT OF THE PRINTING AND TYPESETTING INDUSTRY. LOREM
                                IPSUM HAS BEEN THE INDUSTRY'S STANDARD DUMMY TEXT EVER SPECIMEN BOOK. IT HAS SURVIVED
                                NOT ONLY FIVE CENTURIES, BUT ALSO THE LEAP INTO ELECTRONIC TYPESETTING, REMAINING
                                ESSENTIALLY UNCHANGED. IT WAS POPULARISED IN THE 1960S WITH THE RELEASE OF LETRASET
                                SHEETS CONTAINING LOREM IPSUM PASSAGES.
                            </span>
                        </div>
                    </div>
                </div>
                <div class="carousel-item ">
                    <div class="carrousel-img">
                        <div class="col-lg-7 col-md-7 col-sm-12">
                            <img class="mw-100 image-container" src="img\img-blog 1.png" alt="" />
                        </div>
                        <div class="col-lg-5 col-md-5 col-sm-12 pt-3 description-bloc">
                            <span class="pl-3 span-blog ">
                                LOREM IPSUM IS SIMPLY DUMMY TEXT OF THE PRINTING AND TYPESETTING INDUSTRY. LOREM IPSUM
                                HAS BEEN THE INDUSTRY'S STANDARD DUMMY TEXT EVER SINCE THE 1500S, WHEN AN UNKNOWN
                                PRINTER TOOK A GALLEY OF TYPE AND SCRAMBLED IT TO MAKE A TYPE SPECIMEN BOOK. IT HAS
                                SURVIVED NOT ONLY FIVE CENTURIES, BUT ALSO THE LEAP INTO ELECTRONIC TYPESETTING,
                                REMAINING ESSENTIALLY UNCHANGED. IT WAS POPULARISED IN THE 1960S WITH THE RELEASE OF
                                LETRASET SHEETS CONTAINING LOREM IPSUM PASSAGES, AND MORE RECENTLY WITH DESKTOP
                                PUBLISHING SOFTWARE LIKE ALDUS PAGEMAKER INCLUDING VERSIONS OF LOREM IPSULOREM IPSUM IS
                                SIMPLY DUMMY TEXT OF THE PRINTING AND TYPESETTING INDUSTRY. LOREM IPSUM HAS BEEN THE
                                INDUSTRY'S STANDARD DUMMY TEXT EVERLOREM IPSUM IS SIMPLY DUMMY TEXT OF THE PRINTING AND
                                TYPESETTING INDUSTRY. LOREM IPSUM HAS BEEN THE INDUSTRY'S STANDARD DUMMY TEXT EVER SINCE
                                THE 1500S, WHEN AN UNKNOWN PRINTER TOOK A GALLEY OF TYPE AND SCRAMBLED IT TO MAKE A TYPE
                                SPECIMEN BOOK. IT HAS SURVIVED NOT ONLY FIVE CENTURIES, BUT ALSO THE LEAP INTO
                                ELECTRONIC TYPESETTING, REMAINING ESSENTIALLY UNCHANGED. IT WAS POPULARISED IN THE 1960S
                                WITH THE RELEASE OF LETRASET SHEETS CONTAINING LOREM IPSUM PASSAGES, AND MORE RECENTLY
                                WITH DESKTOP PUBLISHING SOFTWARE LIKE ALDUS PAGEMAKER INCLUDING VERSIONS OF LOREM
                                IPSULOREM IPSUM IS SIMPLY DUMMY TEXT OF THE PRINTING AND TYPESETTING INDUSTRY. LOREM
                                IPSUM HAS BEEN THE INDUSTRY'S STANDARD DUMMY TEXT EVER SPECIMEN BOOK. IT HAS SURVIVED
                                NOT ONLY FIVE CENTURIES, BUT ALSO THE LEAP INTO ELECTRONIC TYPESETTING, REMAINING
                                ESSENTIALLY UNCHANGED. IT WAS POPULARISED IN THE 1960S WITH THE RELEASE OF LETRASET
                                SHEETS CONTAINING LOREM IPSUM PASSAGES.
                            </span>
                        </div>
                    </div>
                </div>
                <div class="carousel-item">
                    <div class="carrousel-img">
                        <div class="col-lg-7 col-md-7 col-sm-12">
                            <img class="mw-100 image-container" src="img\img-blog 1.png" alt="" />
                        </div>
                        <div class="col-lg-5 col-md-5 col-sm-12 pt-3 description-bloc">
                            <span class="pl-3 span-blog ">
                                LOREM IPSUM IS SIMPLY DUMMY TEXT OF THE PRINTING AND TYPESETTING INDUSTRY. LOREM IPSUM
                                HAS BEEN THE INDUSTRY'S STANDARD DUMMY TEXT EVER SINCE THE 1500S, WHEN AN UNKNOWN
                                PRINTER TOOK A GALLEY OF TYPE AND SCRAMBLED IT TO MAKE A TYPE SPECIMEN BOOK. IT HAS
                                SURVIVED NOT ONLY FIVE CENTURIES, BUT ALSO THE LEAP INTO ELECTRONIC TYPESETTING,
                                REMAINING ESSENTIALLY UNCHANGED. IT WAS POPULARISED IN THE 1960S WITH THE RELEASE OF
                                LETRASET SHEETS CONTAINING LOREM IPSUM PASSAGES, AND MORE RECENTLY WITH DESKTOP
                                PUBLISHING SOFTWARE LIKE ALDUS PAGEMAKER INCLUDING VERSIONS OF LOREM IPSULOREM IPSUM IS
                                SIMPLY DUMMY TEXT OF THE PRINTING AND TYPESETTING INDUSTRY. LOREM IPSUM HAS BEEN THE
                                INDUSTRY'S STANDARD DUMMY TEXT EVERLOREM IPSUM IS SIMPLY DUMMY TEXT OF THE PRINTING AND
                                TYPESETTING INDUSTRY. LOREM IPSUM HAS BEEN THE INDUSTRY'S STANDARD DUMMY TEXT EVER SINCE
                                THE 1500S, WHEN AN UNKNOWN PRINTER TOOK A GALLEY OF TYPE AND SCRAMBLED IT TO MAKE A TYPE
                                SPECIMEN BOOK. IT HAS SURVIVED NOT ONLY FIVE CENTURIES, BUT ALSO THE LEAP INTO
                                ELECTRONIC TYPESETTING, REMAINING ESSENTIALLY UNCHANGED. IT WAS POPULARISED IN THE 1960S
                                WITH THE RELEASE OF LETRASET SHEETS CONTAINING LOREM IPSUM PASSAGES, AND MORE RECENTLY
                                WITH DESKTOP PUBLISHING SOFTWARE LIKE ALDUS PAGEMAKER INCLUDING VERSIONS OF LOREM
                                IPSULOREM IPSUM IS SIMPLY DUMMY TEXT OF THE PRINTING AND TYPESETTING INDUSTRY. LOREM
                                IPSUM HAS BEEN THE INDUSTRY'S STANDARD DUMMY TEXT EVER SPECIMEN BOOK. IT HAS SURVIVED
                                NOT ONLY FIVE CENTURIES, BUT ALSO THE LEAP INTO ELECTRONIC TYPESETTING, REMAINING
                                ESSENTIALLY UNCHANGED. IT WAS POPULARISED IN THE 1960S WITH THE RELEASE OF LETRASET
                                SHEETS CONTAINING LOREM IPSUM PASSAGES.
                            </span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="d-flex justify-content-center">
                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                    <span class=""><img class="mw-100" src="img\prev-icon-blog.png" alt="" /></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                    <span class=""><img class="mw-100" src="img\next-icon-blog.png" alt="" /></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>

        </div>

    </div>

    </div>

    <div class="container-fluid ">
        <div class="">
        <h2 class="h2-blog">Salle à manger</h2>
       </div>
        <div class="pt-2">
            <img class="w-100 mw-100 image-container " src="img\img-blog 2.png" alt="" /></span>
        </div>

        <div class="row d-flex pt-5">

            <div class="col-sm-9 col-12 description-bloc pt-3">
                <span class="span-blog">
                    LOREM IPSUM IS SIMPLY DUMMY TEXT OF THE PRINTING AND TYPESETTING INDUSTRY.
                    LOREM IPSUM HAS BEEN THE INDUSTRY'S STANDARD DUMMY TEXT EVER SINCE THE
                    1500S, WHEN AN UNKNOWN PRINTER TOOK A GALLEY OF TYPE AND SCRAMBLED IT TO
                    MAKE A TYPE SPECIMEN BOOK. IT HAS SURVIVED NOT ONLY FIVE CENTURIES, BUT
                    ALSO THE LEAP INTO ELECTRONIC TYPESETTING, REMAINING ESSENTIALLY UNCHANGED.
                    IT WAS POPULARISED IN THE 1960S WITH THE RELEASE OF LETRASET SHEETS
                    CONTAINING LOREM IPSUM PASSAGES, AND MORE RECENTLY WITH DESKTOP PUBLISHING
                    SOFTWARE LIKE ALDUS PAGEMAKER INCLUDING VERSIONS OF LOREM IPSULOREM IPSUM
                    IS SIMPLY DUMMY TEXT OF THE PRINTING AND TYPESETTING INDUSTRY. LOREM IPSUM
                    HAS BEEN THE INDUSTRY'S STANDARD DUMMY TEXT EVERLOREM IPSUM IS SIMPLY DUMMY
                    TEXT OF THE PRINTING AND TYPESETTING INDUSTRY. LOREM IPSUM HAS BEEN THE
                    INDUSTRY'S STANDARD DUMMY TEXT EVER SINCE THE 1500S, WHEN AN UNKNOWN
                    PRINTER TOOK A GALLEY OF TYPE AND SCRAMBLED IT TO MAKE A TYPE SPECIMEN
                    BOOK. IT HAS SURVIVED NOT ONLY FIVE CENTURIES, BUT ALSO THE LEAP INTO
                    ELECTRONIC TYPESETTING, REMAINING ESSENTIALLY UNCHANGED. IT WAS
                    POPULARISED IN THE 1960S WITH THE RELEASE OF LETRASET SHEETS CONTAINING
                    LOREM IPSUM PASSAGES, AND MORE RECENTLY WITH DESKTOP PUBLISHING SOFTWARE
                    LIKE ALDUS PAGEMAKER INCLUDING VERSIONS OF LOREM IPSULOREM IPSUM IS
                    SIMPLY DUMMY TEXT OF THE PRINTING AND TYPESETTING INDUSTRY. LOREM IPSUM
                    HAS BEEN THE INDUSTRY'S STANDARD DUMMY TEXT EVER SPECIMEN BOOK. IT HAS
                    SURVIVED NOT ONLY FIVE CENTURIES, BUT ALSO THE LEAP INTO ELECTRONIC
                    TYPESETTING, REMAINING ESSENTIALLY UNCHANGED. IT WAS POPULARISED IN THE
                    1960S WITH THE RELEASE OF LETRASET SHEETS CONTAINING LOREM IPSUM PASSAGES
                    , AND MORE RECENTLY WITH DESKTOP PUBLISHING SOFTWARE LIKE ALDUS PAGEMAKER
                    INCLUDING VERSIONS OF LOREM IPPRINTER TOOK A GALLEY OF TYPE AND SCRAMBLED IT TO MAKE A TYPE SPECIMEN
                    BOOK. IT HAS SURVIVED NOT ONLY FIVE CENTURIES, BUT ALSO THE LEAP INTO
                    ELECTRONIC TYPESETTING, REMAINING ESSENTIALLY UNCHANGED. IT WAS
                    POPULARISED IN THE 1960S WITH THE RELEASE OF LETRASET SHEETS CONTAINING
                    LOREM IPSUM PASSAGES, AND MORE RECENTLY WITH DESKTOP PUBLISHING SOFTWARE
                    LIKE ALDUS PAGEMAKER INCLUDING VERSIONS OF LOREM IPSULOREM IPSUM IS
                    SIMPLY DUMMY TEXT OF THE PRINTING AND TYPESETTING INDUSTRY. LOREM IPSUM
                    HAS BEEN THE INDUSTRY'S STANDARD DUMMY TEXT EVER SPECIMEN BOOK. IT HAS
                    SURVIVED NOT ONLY FIVE CENTURIES, BUT ALSO THE LEAP INTO ELECTRONIC
                    TYPESETTING, REMAINING ESSENTIALLY UNCHANGED. IT WAS POPULARISED IN THE
                    1960S WITH THE RELEASE OF LETRASET SHEETS CONTAINING LOREM IPSUM PASSAGES
                    , AND MORE RECENTLY WITH DESKTOP PUBLISHING SOFTWARE LIKE ALDUS PAGEMAKER
                    INCLUDING VERSIONS OF LOREM IPPRINTER TOOK A GALLEY OF TYPE AND SCRAMBLED IT TO MAKE A TYPE SPECIMEN
                    BOOK. IT HAS SURVIVED NOT ONLY FIVE CENTURIES, BUT ALSO THE LEAP INTO
                    ELECTRONIC TYPESETTING, REMAINING ESSENTIALLY UNCHANGED. IT WAS
                    POPULARISED IN THE 1960S WITH THE RELEASE OF LETRASET SHEETS CONTAINING
                    LOREM IPSUM PASSAGES.
                </span>
            </div>

            <div class="col-sm-3 col-12 ">
                <img class="w-100 mw-100 col-6" src="img\img-chaise.png" alt="" />
                <img class="w-100 mt-1 mw-100 col-6" src="img\img-table.png" alt="" />
            </div>
        </div>
    </div>
    <?php include("footer.php"); ?>
    <?php include("back_to_top.php"); ?>

    <!-- scsript js -->
    <script src="js/jQuery_v3_2_1.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/contact.js"></script>
     <script src="js/header.js"></script>
</body>